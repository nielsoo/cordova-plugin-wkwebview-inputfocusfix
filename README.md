
This plugin was introduced as a temporary fix for the input focusing issue raised on main WKWebView repositories of Apache and Ionic. As the changes are merged to the main repositories - https://github.com/apache/cordova-plugin-wkwebview-engine/pull/37/ and https://github.com/ionic-team/cordova-plugin-wkwebview-engine/pull/171, the plugin is deprecated and has no effect on fixing any focus issues anymore. 

this is a fork of the abandoned github project at https://github.com/onderceylan/cordova-plugin-wkwebview-inputfocusfix, because the input.focus() fix is still broken in cordova, also the plugin on github is not updated for IOS 13, so I decided to fork this repo

to install go to your cordova root folder and issue: cordova plugin add https://gitlab.com/nielsoo/cordova-plugin-wkwebview-inputfocusfix
